# GitLab Agent for Kubernetes demㅓ

본 프로젝트는 Pull 기반 배포 전략을 사용하는 GitLab Agent for K8S 의 예시 입니다.

다양한 사례들은 다음을 참고하세요. 

- [Auto DevOps integration](https://gitlab.com/gitlab-examples/ops/gitops-demo/hello-world-service)
- [GitOps integration of an application project](https://gitlab.com/gitlab-examples/ops/gitops-demo/hello-world-service-gitops)
- [Cluster Management Project integration](https://gitlab.com/gitlab-examples/ops/gitops-demo/cluster-management)

